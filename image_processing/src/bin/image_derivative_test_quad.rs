// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;
extern crate image;

use std::env;
use std::path::Path;

fn main() {
    use glium::{DisplayBuild, Surface};

    let arg = if env::args().count() == 2 {
            env::args().nth(1).unwrap()
        } else {
            panic!("Please enter an input image")
        };

    let image = image::open(&Path::new(&arg)).unwrap().to_rgba();
    let width = image.width();
    let height = image.height();

    let display = glium::glutin::WindowBuilder::new().with_dimensions(width, height).build_glium().unwrap();

    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    let texture = glium::texture::SrgbTexture2d::with_mipmaps(&display, image, glium::texture::MipmapsOption::NoMipmap).unwrap();
    let sampler = texture.sampled()
                    .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                    .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                    .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp);

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;
            uniform float step_x;
            uniform float step_y;
            uniform int filter_number;

            void main() {
                float gray = dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
                // computes the l1 norm: the sub of the absolute values of the components
                if (filter_number == 0)
                {
                    float l1 = fwidth(gray);
                    color = vec4(l1, l1, l1, 1.0);
                }
                if (filter_number == 1)
                {
                    float dx = dFdx(gray);
                    color = vec4(dx, dx, dx, 1.0);
                }
                if (filter_number == 2)
                {
                    float dy = dFdy(gray);
                    color = vec4(dy, dy, dy, 1.0);
                }
                if (filter_number == 3)
                {
                    float dx =
                        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
                        - dot(texture(tex, v_tex_coords - vec2(step_x, 0.0)).rgb, vec3(0.299, 0.587, 0.114));
                    color = vec4(dx, dx, dx, 1.0);
                }
                if (filter_number == 4)
                {
                    float dy =
                        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
                        - dot(texture(tex, v_tex_coords - vec2(0.0, step_y)).rgb, vec3(0.299, 0.587, 0.114));
                    color = vec4(dy, dy, dy, 1.0);
                }
                if (filter_number == 5)
                {
                    float dx =
                        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
                        - dot(texture(tex, v_tex_coords - vec2(step_x, 0.0)).rgb, vec3(0.299, 0.587, 0.114));
                    float dy =
                        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
                        - dot(texture(tex, v_tex_coords - vec2(0.0, step_y)).rgb, vec3(0.299, 0.587, 0.114));
                    float l1 = abs(dx) + abs(dy);
                    color = vec4(l1, l1, l1, 1.0);
                }
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let mut filter_number = 0;
    let num_filters = 6;

    println!("filter: {}", filter_number);

    loop {
        let uniforms = uniform! {
            tex: sampler,
            step_x: 1.0 / width as f32,
            step_y: 1.0 / height as f32,
            filter_number: filter_number
        };

        let mut target = display.draw();

        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.draw(&vertex_buffer, &indices, &program, &uniforms,
            &Default::default()).unwrap();
        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                glium::glutin::Event::KeyboardInput(glium::glutin::ElementState::Pressed, _, _) => {
                    filter_number = (filter_number + 1) % num_filters;
                    println!("filter: {}", filter_number);
                },
                _ => ()
            }
        }
    }
}