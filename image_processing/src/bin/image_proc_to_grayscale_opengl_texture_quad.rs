// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

/// Works with grayscale images - result looks the same as the original grayscale image.
/// Probably takes only one of the channels as F32 format in case of a colour image.

#[macro_use]
extern crate glium;
extern crate image;

use std::env;
use std::path::Path;

fn main() {
    use glium::{DisplayBuild, Surface};

    let arg = if env::args().count() == 2 {
            env::args().nth(1).unwrap()
        } else {
            panic!("Please enter an input image")
        };

    let image = image::open(&Path::new(&arg)).unwrap().to_rgba();

    let display = glium::glutin::WindowBuilder::new().with_dimensions(image.width(), image.height()).build_glium().unwrap();

    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    //TODO sRGB errors?
    let texture = glium::texture::Texture2d::with_format(
        &display, image, glium::texture::UncompressedFloatFormat::F32, glium::texture::MipmapsOption::NoMipmap, ).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;

            void main() {
                float gray = texture(tex, v_tex_coords).x;
                color = vec4(gray, gray, gray, 1.0);
            }
        "#;

        glium::Program::new(&display,
                            glium::program::ProgramCreationInput::SourceCode {
                                vertex_shader: vertex_shader_src,
                                tessellation_control_shader: None,
                                tessellation_evaluation_shader: None,
                                geometry_shader: None,
                                fragment_shader: fragment_shader_src,
                                transform_feedback_varyings: None,
                                outputs_srgb: true,
                                uses_point_size: false
                            }).unwrap()
    };

    let uniforms = uniform! {
        tex: texture.sampled()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };

    loop {
        let mut target = display.draw();

        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.draw(&vertex_buffer, &indices, &program, &uniforms,
            &Default::default()).unwrap();
        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                _ => ()
            }
        }
    }
}
