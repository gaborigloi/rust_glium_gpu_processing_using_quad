#[macro_use]
extern crate glium;

use std::borrow::Cow;

fn create_texture<F>(facade: &F, data: Option<Vec<f32>>, width: u32, height: u32) -> Result<glium::texture::Texture2d, glium::texture::TextureCreationError>
    where F: glium::backend::Facade {
    match data {
        Some(d) => {
            let data = glium::texture::RawImage2d {
                data: Cow::Owned(d),
                width: width,
                height: height,
                format: glium::texture::ClientFormat::F32
            };
            glium::texture::Texture2d::with_format(
                facade,
                data,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap)
        },
        None => {
            glium::texture::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap,
                width,
                height)
        }
    }
}

fn main() {
    use glium::{DisplayBuild, Surface};

    let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .build_glium()
        .unwrap();

    let data = vec![1.0_f32, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0];

    let (width, height) = (3, 3);

    println!("input: {:?}", data);

    let data = create_texture(&display, Some(data), width, height).unwrap();

    let input: Vec<Vec<f32>> = data.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: data.width(), height: data.height()
        });

    println!("input: {:?}", input);

    let output = create_texture(&display, None, width, height).unwrap();

    let framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            uniform float step_x;
            uniform float step_y;

            in vec2 v_tex_coords;
            out float color;

            uniform sampler2D tex;

            void main() {
                // Below 0.003, we get the same results.
                // The accuracy of the texture coordinates is limited to 1/256:
                // http://gamedev.stackexchange.com/questions/101953/low-quality-bilinear-sampling-in-webgl-opengl-directx/102397#102397
                // http://ww.w.gpucomputing.net/sites/default/files/papers/664/10318.pdf
                //float value = texture(tex, v_tex_coords - vec2(step_x - 0.003, step_y)).x;
                float value = texture(tex, v_tex_coords - vec2(step_x - 0.003, step_y)).x;
                color = value;
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let (step_x, step_y) = (1.0 / width as f32, 1.0 / height as f32);
    println!("step: {:?}", (step_x, step_y));

    // The result is the same, both for Nearest and Linear.
    // The fetched, interpolated value is unaffected by a small change
    // in the coordinates, remains the same as the texel value.
    let uniforms = uniform! {
        tex: data.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Linear)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Linear)
                .wrap_function(glium::uniforms::SamplerWrapFunction::Repeat),
        step_x: step_x,
        step_y: step_y
    };

    let mut target = framebuffer;

    target.draw(&vertex_buffer, &indices, &program, &uniforms,
        &Default::default()).unwrap();

    let output: Vec<Vec<f32>> = output.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: output.width(), height: output.height()
        });

    println!("output: {:?}", output);

    println!("finished");
}