// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;

use std::borrow::Cow;

fn create_texture<F>(facade: &F, data: Option<Vec<f32>>) -> Result<glium::texture::Texture2d, glium::texture::TextureCreationError>
    where F: glium::backend::Facade {
    match data {
        Some(d) => {
            let data = glium::texture::RawImage2d {
                data: Cow::Owned(d),
                width: 2,
                height: 2,
                format: glium::texture::ClientFormat::F32
            };
            // could also just pass a Vec<Vec<_>> here without conversion instead of a RawImage2d
            glium::texture::Texture2d::with_format(
                facade,
                data,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap)
        },
        None => {
            glium::texture::Texture2d::empty_with_format(
                facade,
                glium::texture::UncompressedFloatFormat::F32,
                glium::texture::MipmapsOption::NoMipmap,
                2,
                2)
        }
    }
}

fn print_texture(name: &str, texture: &glium::texture::Texture2d) {
    let output: Vec<Vec<f32>> = texture.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: texture.width(), height: texture.height()
        });

    println!("{}: {:?}", name, output);
}

fn main() {
    use glium::{DisplayBuild, Surface};

    // offscreen
    // does not work for OpenGL 2.1
    /*let display = glium::glutin::HeadlessRendererBuilder::new(1, 1)
        .build_glium().unwrap();*/
    // with window
    let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .with_dimensions(1,1)
        .build_glium()
        .unwrap();

    let data = vec![1.0_f32, 2.0, 3.0, 4.0];

    println!("input: {:?}", data);

    let data = create_texture(&display, Some(data)).unwrap();

    let output1 = create_texture(&display, None).unwrap();
    let output2 = create_texture(&display, None).unwrap();

    let output = [ ("output1", &output1), ("output2", &output2) ];
    let framebuffer = glium::framebuffer::MultiOutputFrameBuffer::new(&display, output.iter().cloned()).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = program!(&display,
        140 => {
            vertex: r#"
                #version 140

                in vec2 position;
                in vec2 tex_coords;
                out vec2 v_tex_coords;

                void main() {
                    v_tex_coords = tex_coords;
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            "#,
            fragment: r#"
                #version 140

                in vec2 v_tex_coords;
                out float output1;
                out float output2;

                uniform sampler2D tex;

                void main() {
                    float value = texture(tex, v_tex_coords).x;
                    output1 = value*value;
                    output2 = value*2.0;
                }
            "#
        },
        110 => {
            vertex: r#"
                #version 110

                attribute vec2 position;
                attribute vec2 tex_coords;
                varying vec2 v_tex_coords;

                void main() {
                    v_tex_coords = tex_coords;
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            "#,
            fragment: r#"
                #version 110

                varying vec2 v_tex_coords;

                uniform sampler2D tex;

                void main() {
                    float value = texture2D(tex, v_tex_coords).x;
                    gl_FragData[0] = vec4(value*value);
                    gl_FragData[1] = vec4(value*2.0);
                }
            "#
        }
    ).unwrap();

    let uniforms = uniform! {
        tex: data.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };

    let mut target = framebuffer;

    target.draw(&vertex_buffer, &indices, &program, &uniforms,
        &Default::default()).unwrap();

    print_texture("output1", &output1);
    print_texture("output2", &output2);

    println!("finished");
}