// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;

fn create_texture<F>(facade: &F, data: Vec<Vec<f32>>) -> Result<glium::texture::texture2d::Texture2d, glium::texture::TextureCreationError>
    where F: glium::backend::Facade {
    glium::texture::Texture2d::with_format(
        facade,
        data,
        glium::texture::UncompressedFloatFormat::F32,
        glium::texture::MipmapsOption::NoMipmap)
}

fn main() {
    use glium::{DisplayBuild, Surface};

    // uses osmesa, which does not support opengl 3
    /*let display = glium::glutin::HeadlessRendererBuilder::new(image.width(), image.height())
        .build_glium().unwrap();*/
    let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .build_glium()
        .unwrap();

    let data = vec![vec![1.0_f32, 2.0],
                    vec![3.0, 4.0]];

    println!("input: {:?}", data);

    let data = create_texture(&display, data).unwrap();

    let output = glium::texture::Texture2d::empty_with_format(
        &display,
        glium::texture::UncompressedFloatFormat::F32,
        glium::texture::MipmapsOption::NoMipmap,
        2,
        2).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out float color;

            uniform sampler2D tex;

            void main() {
                float value = texture(tex, v_tex_coords).x;
                color = value*value;
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! {
        tex: data.sampled()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };

    let mut target = output.as_surface();

    target.draw(&vertex_buffer, &indices, &program, &uniforms,
        &Default::default()).unwrap();

    let uniforms = uniform! {
        tex: output.sampled()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };

    let mut target = data.as_surface();

    target.draw(&vertex_buffer, &indices, &program, &uniforms,
        &Default::default()).unwrap();

    let output: Vec<Vec<f32>> = data.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: output.width(), height: output.height()
        });

    println!("output: {:?}", output);

    println!("finished");
}