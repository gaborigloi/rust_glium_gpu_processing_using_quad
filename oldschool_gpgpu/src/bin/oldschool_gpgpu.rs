/// TODO add this to glium examples -> do gpgpu version of same computation as gpgpu.rs, share, benchmark
/// TODO write a version for older/ES/osmesa GLSL version / DSL / rust/other GLSL library
/// TODO publish glium+rust version of the c/c++ GPGPU tutorial?:
///  * http://www.mathematik.uni-dortmund.de/~goeddeke/gpgpu/
///  * https://www.seas.upenn.edu/~cis565/fbo.htm
///  * Notes:
///     * This is a thing of the past, but: include relevant notes from code/rust/shdr project
///     * Going further: phantom type info for safety, works well with type inference: ^ shdr
///       project, part II project notes, info, code

// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;

use std::borrow::Cow;

fn create_texture<F>(facade: &F, data: Vec<f32>) -> Result<glium::texture::texture2d::Texture2d, glium::texture::TextureCreationError>
    where F: glium::backend::Facade {
    let data = glium::texture::RawImage2d {
        data: Cow::Owned(data),
        width: 2,
        height: 2,
        format: glium::texture::ClientFormat::F32
    };
    // could also just pass a Vec<Vec<_>> here without conversion instead of a RawImage2d
    glium::texture::Texture2d::with_format(
        facade,
        data,
        glium::texture::UncompressedFloatFormat::F32,
        glium::texture::MipmapsOption::NoMipmap)
}

fn main() {
    use glium::{DisplayBuild, Surface};

    /*let display = glium::glutin::HeadlessRendererBuilder::new(image.width(), image.height())
        .build_glium().unwrap();*/
    let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .build_glium()
        .unwrap();

    let data = vec![1.0_f32, 2.0, 3.0, 4.0];
    // to see the rounding errors:
    //let data = vec![0.1_f32, 0.2, 0.3, 0.4];

    println!("input: {:?}", data);

    let data = create_texture(&display, data).unwrap();

    let output = glium::texture::Texture2d::empty_with_format(
        &display,
        glium::texture::UncompressedFloatFormat::F32,
        glium::texture::MipmapsOption::NoMipmap,
        2,
        2).unwrap();

    let framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out float color;

            uniform sampler2D tex;

            void main() {
                // The shader will always sample a 4-component vector,
                // when not all the components stored, the remaining RGB ones will be 0
                // and A will be 1:
                // https://www.opengl.org/wiki/Image_Format#Color_formats
                float value = texture(tex, v_tex_coords).x;
                color = value*value;
            }
        "#;

        //TODO write osmesa (Opengl 2.1) version

        // Not necessary to set outputs_srgb to false, since the target texture is in RGB,
        // not in sRGB, so no sRGB conversion is necessary in this case, regardless of the value of outputs_srgb.
        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
        /*glium::Program::new(&display,
                            glium::program::ProgramCreationInput::SourceCode {
                                vertex_shader: vertex_shader_src,
                                tessellation_control_shader: None,
                                tessellation_evaluation_shader: None,
                                geometry_shader: None,
                                fragment_shader: fragment_shader_src,
                                transform_feedback_varyings: None,
                                outputs_srgb: true,
                                uses_point_size: false
                            }).unwrap()*/
    };

    let uniforms = uniform! {
        tex: data.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };

    let mut target = framebuffer;


    // Should the corresponding color mask components be disabled when some components
    // are missing from the texture (for example in case of a texture with F32 format)?
    target.draw(&vertex_buffer, &indices, &program, &uniforms,
        &Default::default()).unwrap();

    let output: Vec<Vec<f32>> = output.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: output.width(), height: output.height()
        });

    println!("output: {:?}", output);

    println!("finished");
}
