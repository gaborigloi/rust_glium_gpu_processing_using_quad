//! Tests different possible format errors in GPGPU computation,
//! when uploading or downloading textures.

#[macro_use]
extern crate glium;

use std::fmt::Debug;

fn try_format<'a, F, T1, P2, T2>(
    facade: &F,
    input_data: T1,
    texture_format: glium::texture::UncompressedFloatFormat) -> T2
    where F: glium::backend::Facade,
          T1: glium::texture::Texture2dDataSource<'a> + Debug,
          T2: glium::texture::Texture2dDataSink<P2> + Debug, P2: glium::texture::PixelValue {

    println!("input: {:?}", input_data);

    let texture = glium::texture::Texture2d::with_format(
        facade,
        input_data,
        texture_format,
        glium::texture::MipmapsOption::NoMipmap).unwrap();

    let output_data = texture.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: texture.width(), height: texture.height()
        });

    println!("output: {:?}", output_data);

    output_data
}

/*

fn try_format_with_raw_image<F, P1, T1, P2, T2>(
    facade: &F,
    input_data: T1,
    client_format: glium::texture::ClientFormat,
    texture_format: glium::texture::UncompressedFloatFormat,
    output_data: T2) -> T2
    where F: glium::backend::Facade,
          T1: glium::texture::Texture2dDataSource<P1>, P1: glium::texture::PixelValue,
          T2: glium::texture::Texture2dDataSink<P2>, P2: glium::texture::PixelValue {

    use std::borrow::Cow;

    let raw_image = glium::texture::RawImage2d {
        data: Cow::Owned(input_data),
        width: 2,
        height: 2,
        format: client_format
    };

    // call try_format
}*/

fn main() {
    use glium::DisplayBuild;

    // offscreen
    let display = glium::glutin::HeadlessRendererBuilder::new(1, 1)
        .build_glium().unwrap();
    // with window
    /*let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .with_dimensions(1, 1)
        .build_glium()
        .unwrap();*/

    let data_f32f32 = vec![
                vec![(1.0_f32, 1.5_f32), (2.0, 2.5)],
                vec![(3.0, 3.5), (4.0, 4.5)]];
    let data_f32 = vec![
                vec![1.0_f32, 2.0],
                vec![3.0, 4.0]];

    /* When a RawImage2d is used, uploading data with the wrong format that
     * does not match the client format causes a panic.
     */

    // Test implementation:
    // Could generate these + texture_vs_shader_format_test.rs using macros, from (f32,f32),F32F32F32,(f32,f32,f32) fromats.
    // A SampleData::create<Format>() function could be used to create sample data in the macro with the specified format.
    //
    println!("\nextended matching formats");
    println!("works with osmesa (HeadlessRenderer, only supporting OpenGL 2.1):");
    println!("(f32,f32)->F32F32F32->(f32,f32,f32)");
    let _: Vec<Vec<(f32, f32, f32)>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32F32);

    println!("\nmatching formats");
    println!("reads corrupted data with osmesa:");
    println!("(f32,f32)->F32F32->(f32,f32)");
    let _: Vec<Vec<(f32, f32)>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32);

    println!("\nwrong (smaller) output format - second half of data discarded");
    println!("works with osmesa:");
    println!("(f32,f32)->F32F32->f32");
    let _: Vec<Vec<f32>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32);

    // reads corrupted data with osmesa
    println!("\nwrong (smaller) texture format - missing data");
    println!("reads corrupted data with osmesa:");
    println!("(f32,f32)->F32->(f32,f32)");
    let _: Vec<Vec<(f32,f32)>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32);
    println!("works with osmesa:");
    println!("(f32,f32)->F32->f32");
    let _: Vec<Vec<f32>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32);

    // reads corrupted data with osmesa
    println!("\nwrong (smaller) input format - missing data");
    println!("reads corrupted data with osmesa:");
    println!("f32->F32F32->(f32, f32)");
    let _: Vec<Vec<(f32,f32)>> =
        try_format(&display,
                   data_f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32);
}