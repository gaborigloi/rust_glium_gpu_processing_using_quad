//! Tests different possible format errors in GPGPU computation,
//! when drawing to textures.
//TODO report glium error as error messages says?

#[macro_use]
extern crate glium;

use std::fmt::Debug;
use glium::Surface;

fn try_format<'a, F, T1, P2, T2>(
    facade: &F,
    input_data: T1,
    input_texture_format: glium::texture::UncompressedFloatFormat,
    pixel_shader_src: (&str, &str),
    output_texture_format: glium::texture::UncompressedFloatFormat) -> T2
    where F: glium::backend::Facade,
          T1: glium::texture::Texture2dDataSource<'a> + Debug,
          T2: glium::texture::Texture2dDataSink<P2> + Debug, P2: glium::texture::PixelValue {

    println!("input: {:?}", input_data);

    let input_texture = glium::texture::Texture2d::with_format(
        facade,
        input_data,
        input_texture_format,
        glium::texture::MipmapsOption::NoMipmap).unwrap();

    let output_texture = glium::texture::Texture2d::empty_with_format(
        facade,
        output_texture_format,
        glium::texture::MipmapsOption::NoMipmap,
        input_texture.width(),
        input_texture.height()).unwrap();

    //**************************** create program, quad

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(facade, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = program!(facade,
        140 => {
            vertex: include_str!("../shaders/pixel.vert"),
            fragment: pixel_shader_src.0
        },
        110 => {
            vertex: include_str!("../shaders/pixel_osmesa.vert"),
            fragment: pixel_shader_src.1
        },
    ).unwrap();

    let uniforms = uniform! {
        tex: input_texture.sampled()
                .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };
    //********************************************quad, program created

    output_texture.as_surface().draw(&vertex_buffer, &indices, &program, &uniforms,
        &Default::default()).unwrap();

    let output_data = output_texture.main_level()
        .first_layer()
        .into_image(None).unwrap()
        .raw_read(&glium::Rect {
            left: 0, bottom: 0, width: output_texture.width(), height: output_texture.height()
        });

    println!("output: {:?}", output_data);

    output_data
}

fn main() {
    use glium::DisplayBuild;

    // offscreen
    /*let display = glium::glutin::HeadlessRendererBuilder::new(1, 1)
        .build_glium().unwrap();*/
    // with window
    let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .with_dimensions(1, 1)
        .build_glium()
        .unwrap();

    let data_f32f32f32 = vec![
                vec![(1.0_f32, 2.0_f32, 3.0_f32), (4.0, 5.0, 6.0)],
                vec![(7.0, 8.0, 9.0), (10.0, 11.0, 12.0)]];
    let data_f32f32 = vec![
                vec![(1.0_f32, 2.0_f32), (3.0, 4.0)],
                vec![(5.0, 6.0), (7.0, 8.0)]];
    let data_f32 = vec![
                vec![1.0_f32, 2.0],
                vec![3.0, 4.0]];

    let fragment_shader_float = (r#"
        #version 140

        in vec2 v_tex_coords;
        out float color;

        uniform sampler2D tex;

        void main() {
            float value = texture(tex, v_tex_coords).r;
            color = value*value;
        }
    "#,
    r#"
        #version 110

        varying vec2 v_tex_coords;

        uniform sampler2D tex;

        void main() {
            float value = texture2D(tex, v_tex_coords).r;
            // can only return vec4
            gl_FragColor = vec4(value*value);
        }
    "#);

    let fragment_shader_vec2 = (r#"
        #version 140

        in vec2 v_tex_coords;
        out vec2 color;

        uniform sampler2D tex;

        void main() {
            vec2 value = texture(tex, v_tex_coords).rg;
            color = value*value;
        }
    "#,
    r#"
        #version 110

        varying vec2 v_tex_coords;

        uniform sampler2D tex;

        void main() {
            vec2 value = texture2D(tex, v_tex_coords).rg;
            // can only return vec4
            gl_FragColor = vec4(value*value, 0.0, 0.0);
        }
    "#);

    println!("\nextended matching formats");
    println!("works with osmesa (HeadlessRenderer, only supporting OpenGL 2.1):");
    println!("f32->F32F32F32->float->F32F32F32->(f32,f32,f32)");
    let _: Vec<Vec<(f32, f32, f32)>> =
        try_format(&display,
                   data_f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32F32,
                   fragment_shader_float,
                   glium::texture::UncompressedFloatFormat::F32F32F32);
    println!("works with osmesa:");
    println!("(f32,f32)->F32F32F32->vec2->F32F32F32->(f32,f32,f32)");
    let _: Vec<Vec<(f32, f32, f32)>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32F32,
                   fragment_shader_vec2,
                   glium::texture::UncompressedFloatFormat::F32F32F32);

    println!("\nmatching formats");
    println!("reads corrupted data with osmesa:");
    println!("f32->F32->float->F32->f32");
    let _: Vec<Vec<f32>> =
        try_format(&display,
                   data_f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32,
                   fragment_shader_float,
                   glium::texture::UncompressedFloatFormat::F32);
    println!("reads corrupted data with osmesa:");
    println!("(f32,f32)->F32F32->vec2->F32F32->(f32,f32)");
    let _: Vec<Vec<(f32, f32)>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32,
                   fragment_shader_vec2,
                   glium::texture::UncompressedFloatFormat::F32F32);

    println!("\nwrong (larger) input format - second half of data discarded");
    println!("reads corrupted data with osmesa:");
    println!("(f32,f32)->F32F32->float->F32->f32");
    let _: Vec<Vec<f32>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32,
                   fragment_shader_float,
                   glium::texture::UncompressedFloatFormat::F32);
    println!("reads corrupted data with osmesa:");
    println!("(f32,f32,f32)->F32F32F32->vec2->F32F32->(f32,f32)");
    let _: Vec<Vec<(f32,f32)>> =
        try_format(&display,
                   data_f32f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32F32,
                   fragment_shader_vec2,
                   glium::texture::UncompressedFloatFormat::F32F32);

    println!("\nwrong (larger) output format - missing data");
    println!("reads corrupted data with osmesa:");
    println!("f32->F32->float->F32F32->(f32,f32)");
    let _: Vec<Vec<(f32,f32)>> =
        try_format(&display,
                   data_f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32,
                   fragment_shader_float,
                   glium::texture::UncompressedFloatFormat::F32F32);
    println!("works with osmesa:");
    println!("(f32,f32)->F32F32->vec2->F32F32F32->(f32,f32,f32)");
    let _: Vec<Vec<(f32,f32,f32)>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32,
                   fragment_shader_vec2,
                   glium::texture::UncompressedFloatFormat::F32F32F32);

    println!("\nwrong (smaller) output format");
    println!("reads corrupted data with osmesa:");
    println!("(f32,f32)->F32F32->vec2->F32->f32");
    let _: Vec<Vec<f32>> =
        try_format(&display,
                   data_f32f32.clone(),
                   glium::texture::UncompressedFloatFormat::F32F32,
                   fragment_shader_vec2,
                   glium::texture::UncompressedFloatFormat::F32);
}