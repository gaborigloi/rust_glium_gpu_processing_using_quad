/// input: a grayscale texture

#version 140

in vec2 v_tex_coords;
out float blurred;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    vec2 sx = vec2(step_x, 0.0);
    vec2 sy = vec2(step_y, 0.0);
    vec2 c = v_tex_coords;
    blurred =
        0.05472157 * texture(tex, c - sx + sy).r + 0.11098164 * texture(tex, c + sy).r + 0.05472157 * texture(tex, c + sx + sy).r +
        0.11098164 * texture(tex, c - sx).r      + 0.22508352 * texture(tex, c).r      + 0.11098164 * texture(tex, c + sx).r      +
        0.05472157 * texture(tex, c - sx - sy).r + 0.11098164 * texture(tex, c - sy).r + 0.05472157 * texture(tex, c + sx - sy).r;
}