// numbers generated using octave: f_gauss = fspecial("gaussian", 5, 2)

#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    vec2 sx = vec2(step_x, 0.0);
    vec2 sy = vec2(step_y, 0.0);
    vec2 c = v_tex_coords;
    color =
       0.023247 * texture(tex, c - 2*sx + 2*sy) + 0.033824 * texture(tex, c - sx + 2*sy) + 0.038328 * texture(tex, c + 2*sy) + 0.033824 * texture(tex, c + sx + 2*sy) + 0.023247 * texture(tex, c + 2*sx + 2*sy) +
       0.033824 * texture(tex, c - 2*sx + sy)   + 0.049214 * texture(tex, c - sx + sy)   + 0.055766 * texture(tex, c + sy)   + 0.049214 * texture(tex, c + sx + sy)   + 0.033824 * texture(tex, c + 2*sx + sy)   +
       0.038328 * texture(tex, c - 2*sx)        + 0.055766 * texture(tex, c - sx)        + 0.063191 * texture(tex, c)        + 0.055766 * texture(tex, c + sx)        + 0.038328 * texture(tex, c + 2*sx)        +
       0.033824 * texture(tex, c - 2*sx - sy)   + 0.049214 * texture(tex, c - sx - sy)   + 0.055766 * texture(tex, c - sy)   + 0.049214 * texture(tex, c + sx - sy)   + 0.033824 * texture(tex, c + 2*sx - sy)   +
       0.023247 * texture(tex, c - 2*sx - 2*sy) + 0.033824 * texture(tex, c - sx - 2*sy) + 0.038328 * texture(tex, c - 2*sy) + 0.033824 * texture(tex, c + sx - 2*sy) + 0.023247 * texture(tex, c + 2*sx - 2*sy);
}