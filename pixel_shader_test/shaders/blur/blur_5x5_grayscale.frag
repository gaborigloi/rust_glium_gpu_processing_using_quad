/// input: a grayscale texture
///
/// numbers generated using octave: f_gauss = fspecial("gaussian", 5, 2)

#version 140

in vec2 v_tex_coords;
out float blurred;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    vec2 sx = vec2(step_x, 0.0);
    vec2 sy = vec2(step_y, 0.0);
    vec2 c = v_tex_coords;
    blurred =
       0.023247 * texture(tex, c - 2*sx + 2*sy).r + 0.033824 * texture(tex, c - sx + 2*sy).r + 0.038328 * texture(tex, c + 2*sy).r + 0.033824 * texture(tex, c + sx + 2*sy).r + 0.023247 * texture(tex, c + 2*sx + 2*sy).r +
       0.033824 * texture(tex, c - 2*sx + sy).r   + 0.049214 * texture(tex, c - sx + sy).r   + 0.055766 * texture(tex, c + sy).r   + 0.049214 * texture(tex, c + sx + sy).r   + 0.033824 * texture(tex, c + 2*sx + sy).r   +
       0.038328 * texture(tex, c - 2*sx).r        + 0.055766 * texture(tex, c - sx).r        + 0.063191 * texture(tex, c).r        + 0.055766 * texture(tex, c + sx).r        + 0.038328 * texture(tex, c + 2*sx).r        +
       0.033824 * texture(tex, c - 2*sx - sy).r   + 0.049214 * texture(tex, c - sx - sy).r   + 0.055766 * texture(tex, c - sy).r   + 0.049214 * texture(tex, c + sx - sy).r   + 0.033824 * texture(tex, c + 2*sx - sy).r   +
       0.023247 * texture(tex, c - 2*sx - 2*sy).r + 0.033824 * texture(tex, c - sx - 2*sy).r + 0.038328 * texture(tex, c - 2*sy).r + 0.033824 * texture(tex, c + sx - 2*sy).r + 0.023247 * texture(tex, c + 2*sx - 2*sy).r;
}