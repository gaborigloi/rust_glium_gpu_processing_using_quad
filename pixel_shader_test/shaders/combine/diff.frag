#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex1;
uniform sampler2D tex2;

void main() {
    color = abs(texture(tex1, v_tex_coords) - texture(tex2, v_tex_coords));
}