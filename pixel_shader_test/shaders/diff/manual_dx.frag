#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    float dx =
        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
        - dot(texture(tex, v_tex_coords - vec2(step_x, 0.0)).rgb, vec3(0.299, 0.587, 0.114));
    color = vec4(dx, dx, dx, 1.0);
}