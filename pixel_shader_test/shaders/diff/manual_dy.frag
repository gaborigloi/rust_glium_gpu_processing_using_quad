#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    float gray = dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
    float dy =
        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
        - dot(texture(tex, v_tex_coords - vec2(0.0, step_y)).rgb, vec3(0.299, 0.587, 0.114));
    color = vec4(dy, dy, dy, 1.0);
}