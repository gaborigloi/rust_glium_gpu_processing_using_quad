#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    // computes the l1 norm: the sub of the absolute values of the components
    float dx =
        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
        - dot(texture(tex, v_tex_coords - vec2(step_x, 0.0)).rgb, vec3(0.299, 0.587, 0.114));
    float dy =
        dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114))
        - dot(texture(tex, v_tex_coords - vec2(0.0, step_y)).rgb, vec3(0.299, 0.587, 0.114));
    float l1 = abs(dx) + abs(dy);
    color = vec4(l1, l1, l1, 1.0);
}