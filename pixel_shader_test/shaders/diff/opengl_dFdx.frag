#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

void main() {
    float gray = dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
    float dx = dFdx(gray);
    color = vec4(dx, dx, dx, 1.0);
}