#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;
uniform float step_x;
uniform float step_y;

// computes the l1 norm: the sub of the absolute values of the components
void main() {
    float gray = dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
    float l1 = fwidth(gray);
    color = vec4(l1, l1, l1, 1.0);
}