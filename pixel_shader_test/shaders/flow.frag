// http://cs.brown.edu/courses/csci1290/2011/results/final/psastras/
#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D Ix1;
uniform sampler2D Iy1;
uniform sampler2D I1, I2;

uniform float step_x;
uniform float step_y;

float ix1(vec2 p) {
    return texture(I1, vec2(-1.0,y1)*step)

void main() {

   float G0, G1, G2;

   vec2 step = vec2(step_x, step_y);

   vec2 uv = vec2(0.0, 0.0);

   G0 = 0.0045; G1 = 0.0; G2 = 0.0045; //prevent div by zero

   for(int j=0; j<10; j++) { //for num iterations

       vec2 coords_warped = v_tex_coords + uv;

       float v0 = 0.0; float v1 = 0.0; //image mismatch vector

       for(int k=-2; k<=2; k++) {

           float x1 = v_tex_coords.x+float(k)*step_x;

           float x2 = ytop_w+float(k)+0.5;

           for(int l=-2; l<=2;l++) {

                float y1 = ytop+float(l);

                //if we just have this in 1 rgba texture we could reduce this to one tex lookup?

                float A_t1 = texture(Ix1, coords_warped + vec2(x1,y1)*step);

                float A_t2 = texture(Iy1, coords_warped + vec2(x1,y1)*step);

                float dI   = texture(I1, coords_warped + vec2(x1,y1)*step) -

                             texture(I2, coords_warped + vec2(x2,y2)*step);

                if(j==0) { //compute G

                    G0 += A_t1*A_t1; //Ix^2

                    G1 += A_t1*A_t2; //Ixy

                    G2 += A_t2*A_t2; //Iy^2

                }

                v0 += A_t1 * dI; v1 += A_t2 * dI; //image mismatch vector

           }

           float det_inv = 1.0 / (G0 * G2 - G1 * G1);

           float G00 = G0;

           G0 = G2 * det_inv;  G1 *= -det_inv; G2 = G00 * det_inv; //G^-1

       }

       //v^k=v^k-1+(G^-1b_k)

       uv.x += v0*G0+v1*G1;

       uv.y += v0*G1+v1*G2;

   }

   color = vec4(uv.x, uv.y, 0.0,0.0);
}
