#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;

void main() {
    // derive luminance from R, G, B co-ordinates
    float gray = dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
    color = vec4(gray, gray, gray, 1.0);
}