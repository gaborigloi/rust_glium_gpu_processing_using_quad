#version 140

in vec2 v_tex_coords;
out vec4 color;

uniform sampler2D tex;

void main() {
    for (int i = -9; i <= 9; i++) {
        for (int j = -9; j <= 9; j++) {
            sum += texture(tex, v_tex_coords + vec2(float(i) * step_x, float(j) * step_y)).r;
        }
    }
    color = texture(tex, v_tex_coords);
}
