// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;
extern crate glium_uniforms;
extern crate image;

use std::env;
use std::path::Path;
use std::fs::File;
use std::io::Read;

use glium_uniforms::*;

fn create_texture<F>(facade: &F, image: image::RgbaImage) -> glium::texture::SrgbTexture2d
    where F: glium::backend::Facade
{
    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(),
                                                                   image_dimensions);
    glium::texture::SrgbTexture2d::with_mipmaps(facade,
                                                image,
                                                glium::texture::MipmapsOption::NoMipmap)
        .unwrap()
}

fn get_sampler(texture: &glium::texture::SrgbTexture2d)
               -> glium::uniforms::Sampler<glium::texture::SrgbTexture2d> {
    texture.sampled()
           .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
           .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
           .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
}

fn main() {
    use glium::{DisplayBuild, Surface};

    if env::args().count() < 3 {
        // using http://docopt.org/ description:
        panic!(r#"
Please enter a pixel (fragment) shader and at least one input image
Usage: test_multi_input_pixel_shader_quad <fragment shader> <image>..."#);
    }

    let arg_shader = env::args().nth(1).unwrap();
    let images = env::args().skip(2);

    let images = images.map(|i| image::open(&Path::new(&i)).unwrap().to_rgba()).collect::<Vec<_>>();
    let width = images[0].width();
    let height = images[0].height();

    let display = glium::glutin::WindowBuilder::new()
                      .with_dimensions(width, height)
                      .build_glium()
                      .unwrap();

    let textures: Vec<_> = images.iter().cloned().map(|i| create_texture(&display, i)).collect();
    let samplers = textures.iter().map(|t| get_sampler(t));
    let sampler_uniforms: Vec<_> = samplers.enumerate()
                                           .map(|(i, s)| (format!("tex{}", i + 1), s))
                                           .collect();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 0.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 1.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = include_str!("../shaders/pixel.vert");

        let mut fragment_shader_src = String::new();
        File::open(&arg_shader)
            .expect("Cannot open shader file")
            .read_to_string(&mut fragment_shader_src)
            .expect("Cannot read shader file");

        glium::Program::from_source(&display, vertex_shader_src, &fragment_shader_src, None)
            .unwrap()
    };

    let uniforms = CombinedUniforms::new(uniform! {
                                            step_x: 1.0 / width as f32,
                                            step_y: 1.0 / height as f32
                                        },
                                         UniformsArray::new(&sampler_uniforms));

    loop {

        let mut target = display.draw();

        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.draw(&vertex_buffer,
                    &indices,
                    &program,
                    &uniforms,
                    &Default::default())
              .unwrap();
        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                _ => (),
            }
        }
    }
}
