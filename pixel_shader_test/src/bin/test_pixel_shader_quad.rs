// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;
extern crate image;

use std::env;
use std::path::Path;
use std::fs::File;
use std::io::Read;

fn main() {
    use glium::{DisplayBuild, Surface};

    if env::args().count() != 3 {
        panic!(r#"
Please enter a pixel (fragment) shader and an input image
Usage: test_pixel_shader_quad <fragment shader> <image>"#);
    }

    let arg_shader = env::args().nth(1).unwrap();
    let arg_image = env::args().nth(2).unwrap();

    let image = image::open(&Path::new(&arg_image)).unwrap().to_rgba();
    let width = image.width();
    let height = image.height();

    let display = glium::glutin::WindowBuilder::new().with_dimensions(width, height).build_glium().unwrap();

    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    let texture = glium::texture::SrgbTexture2d::with_mipmaps(&display, image, glium::texture::MipmapsOption::NoMipmap).unwrap();
    let sampler = texture.sampled()
                    .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                    .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                    .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp);

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = include_str!("../shaders/pixel.vert");

        // http://r3dux.org/2011/06/glsl-image-processing/
        let mut fragment_shader_src = String::new();
        File::open(&arg_shader).expect("Cannot open shader file")
            .read_to_string(&mut fragment_shader_src).expect("Cannot read shader file");

        glium::Program::from_source(&display, vertex_shader_src, &fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! {
        tex: sampler,
        step_x: 1.0 / width as f32,
        step_y: 1.0 / height as f32
    };

    loop {

        let mut target = display.draw();

        target.clear_color(0.0, 0.0, 1.0, 1.0);
        target.draw(&vertex_buffer, &indices, &program, &uniforms,
            &Default::default()).unwrap();
        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                _ => ()
            }
        }
    }
}
