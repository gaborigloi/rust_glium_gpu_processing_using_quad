// modified code from glium examples: https://github.com/tomaka/glium
// (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
// license: https://github.com/tomaka/glium/blob/master/LICENSE

#[macro_use]
extern crate glium;
extern crate image;

use std::thread;
use std::time::Duration;

use std::io::Cursor;

fn main() {
    use glium::{DisplayBuild, Surface};

    let image = image::load(Cursor::new(&include_bytes!("../../resources/parrot_colour.jpg")[..]),
                            image::JPEG).unwrap().to_rgba();

    let display = glium::glutin::WindowBuilder::new().with_dimensions(image.width(), image.height()).build_glium().unwrap();

    let image_dimensions = image.dimensions();
    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    let texture = glium::texture::SrgbTexture2d::with_mipmaps(&display, image, glium::texture::MipmapsOption::NoMipmap).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;

            void main() {
                color = texture(tex, v_tex_coords);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! { tex: &texture };

    loop {

        // This significantly reduces the CPU usage, compared to the simple loop in quad.rs.

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                glium::glutin::Event::Refresh => {
                    let mut target = display.draw();

                    target.clear_color(0.0, 0.0, 1.0, 1.0);
                    target.draw(&vertex_buffer, &indices, &program, &uniforms,
                        &Default::default()).unwrap();
                    target.finish().unwrap();
                },
                _ => ()
            }
        }

        // can't wait for too long, the thread won't be woken up during sleep
        thread::sleep(Duration::from_millis(100));
    }
}